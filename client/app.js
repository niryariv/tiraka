var TILE_URL = "https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoibmlyeWFyaXYiLCJhIjoiQjdJeWdqZyJ9.pZOwn6stABGoptmp0DH1wg";
var SERVER = 'http://0.0.0.0:5000/';

var CONTEXT = {};
var LAYERS = [];
// var GEOJSON = { type: 'FeatureCollection', features: [] };
// var user_layers = new L.LayerGroup();

L.MakiMarkers.accessToken = "pk.eyJ1IjoibmlyeWFyaXYiLCJhIjoiQjdJeWdqZyJ9.pZOwn6stABGoptmp0DH1wg";
var icon = L.MakiMarkers.icon({icon: "playground", color: "#85AFDF", size: "m"});

var map = L.map('map', {
  scrollWheelZoom: true,
  preferCanvas: true // for leaflet-image
});

// for NEXT
// var roads = L.gridLayer.googleMutant({
//     type: 'roadmap' // valid values are 'roadmap', 'satellite', 'terrain' and 'hybrid'
// }).addTo(map);

L.tileLayer(TILE_URL).addTo(map);
map.setView([0,0],2);

$(function() {
  user_text_activate();
});

function user_text_activate(){
  $(".user_text").keypress(
    function (e) {
      if(e.which == 13) {
        e.preventDefault();
        var q = $(".user_text").val();
        console.log('QUERY:' + q);
        // get_data(q);
        run_query(q);

        $(".user_text").addClass("frozen");
        $(".user_text").attr("disabled","disabled");
        $(".user_text").removeClass("user_text");
      }
    });

  autosize($(".user_text"));
  $(".user_text").focus();
}


function onEachFeature(feature, layer) {
  var popup = "<table>";
  var p = feature.properties.tags;

  if (p.hasOwnProperty('name')) {
    popup += "<tr><td colspan=2><b>" + p.name + "</b></td></tr>";
  }
  for (var key in p) {
    if (p.hasOwnProperty(key)) {
      console.log(key + " -> " + p[key]);
      popup += "<tr><td>" + key + "</td><td>" + p[key] + "</td></tr>";
    }
  }
  popup += "</table>";

  L.geoJSON(feature, {style: {"weight": 1, "color": "blue", "fillColor": "red", "icon": icon}}).bindTooltip(popup).addTo(map);
}


function reply(message) {
  $("#dialog").append(
    '<span class="bot_text">'+message+'</span>' +
    '<textarea class="pure-input-1 user_text" type="text" rows="1"></textarea>'
  );
  user_text_activate();
}


geoJson2heat = function(geojson) {
  return geojson.features.map(
    function(feature) {
      if (feature.geometry.type!="Point") feature = turf.centroid(feature);
      return [parseFloat(feature.geometry.coordinates[1]), parseFloat(feature.geometry.coordinates[0])];
    }
  );
}


function parse_query(query){
    return $.getJSON(SERVER + "parse_query/" + query)
}

function get_location(query_response) {
  loc = CONTEXT.entities.location;
  console.log("get_location:" , loc)
  return $.getJSON(SERVER + "get_location/" + loc);
}

function intent_show_objects(nesw,object_type) {
  var n = nesw[0],
      e = nesw[1],
      s = nesw[2],
      w = nesw[3];
  console.log(SERVER + "intent_show_objects/" + n +','+ e +','+ s +','+ w +','+ object_type);
  return $.getJSON(SERVER + "intent_show_objects/" + n +','+ e +','+ s +','+ w +','+ object_type);
}

function update_context(d){
  console.log(d);
  CONTEXT['intent'] = d.intent;
  if (!CONTEXT.hasOwnProperty('entities')) CONTEXT['entities'] = {};
  for (k in d.entities) { // do it this way so we don't overwrite existing entities from previous queries
    CONTEXT['entities'][k] = d.entities[k];
  }
  // CONTEXT['entities'] = d.entities;
}

function update_location(d){
  console.log(d);
  CONTEXT['location'] = {
    'proper_name' : d['name'],
    'nesw'        : [d['n'],d['e'],d['s'],d['w']], //CLEAN UP
    'latlng'      : [d['lat'], d['lng']]
  }
}

function exec_intent(){
  console.log("intent", CONTEXT['intent']);
  switch (CONTEXT['intent']){
    case 'Show Objects':
      return (intent_show_objects(CONTEXT['location']['nesw'], CONTEXT['entities']['object_type'])).then(update_map);
      break;

    case 'export':
      return export_data(CONTEXT['entities']['export_format']);
      break;

    case 'reset':
      return reset_map();
      break;

    default:
      throw new Error ("Didn't get what you mean");
  }
}


function export_data(format){
  var filename = "export";

  if (format == "geojson" || format == "json" || format == "") {
    // compile all layers to a single geoJSON element
    var out_geojson = { type: 'FeatureCollection', features: [] };
    for (var i = 0; i < LAYERS.length; i++) {
      f = LAYERS[i].toGeoJSON().features;
      for (var l = 0; l < f.length; l++) {
        out_geojson.features.push(f[l]);
      }
    }

    download(
      JSON.stringify(out_geojson),
      filename + ".geojson",
      'application/json'
    );
    reply("Downloaded as \"" + filename + "\"");
  }
  else if (format == "image" || format == "png") {
    leafletImage(map, function(err, canvas) {
      download(
        canvas.toDataURL(),
        filename + ".png",
        "image/png"
      );
    });
    reply("Downloaded as \"" + filename + "\"");
  }
}

function reset_map(){
  map.eachLayer(function(l){map.removeLayer(l)});
  L.tileLayer(TILE_URL).addTo(map);
}

function update_map(data){
  console.log(data);

  osm_data = JSON.parse(data.osm_data);

  if (CONTEXT.entities.render_style == 'heatmap') {
    var geoData = geoJson2heat(osm_data);
    var heatMap = new L.heatLayer(geoData,{radius: 30, blur: 25, maxZoom: 17});
    map.addLayer(heatMap);
    var l = L.geoJSON(osm_data);
  } else {
    var l = L.geoJSON(osm_data, { onEachFeature: onEachFeature });
  }

  LAYERS.push(l);
  map.fitBounds(l.getBounds());

  console.log("Features found:" + osm_data.features.length);
  reply(
    "Found " + osm_data.features.length + " " + CONTEXT['entities']['object_type'] + " in " + CONTEXT['location']['proper_name'] +
    " with a total area of " + turf.area(osm_data).toFixed(2) + "sqm"
  );
}


function run_query(query){
  var spinner = new Spinner({scale:6, color:'#cc0'}).spin();
  document.getElementById('map').appendChild(spinner.el);

    return parse_query(query)
      .then(update_context)
      .then(get_location)
      .then(update_location)
      .then(exec_intent)
      .done(function(){
        spinner.stop();
      })
      .catch(function(err) {
        spinner.stop();
        console.log(err);
        reply("Could you try to rephrasing please? I'm just a dumb prototype :( <p>(If it's important, email my creator at <a href=\"mailto:niryariv@gmail.com\">niryariv@gmail.com</a>)</p>");
      })
}

function get_data(query) {
  console.log("getting geojson");

  var spinner = new Spinner({scale:6, color:'#cc0'}).spin();
  document.getElementById('map').appendChild(spinner.el);

  // parseQuery(query).then(getLocation(location)).then(getData(dat))
  $.getJSON("http://0.0.0.0:5000/query/" + query,
    function(data) {
      spinner.stop();
      console.log(data);

      if (data.hasOwnProperty('message')) {
        return(reply(data['message']));
      }

      osm_data = JSON.parse(data.osm_data);

      console.log(osm_data);

      if (data.context.render_style == 'heatmap') {
        var geoData = geoJson2heat(osm_data);
        var heatMap = new L.heatLayer(geoData,{radius: 30, blur: 25, maxZoom: 17});
        map.addLayer(heatMap);
        var l = L.geoJSON(osm_data);
      } else {
        var l = L.geoJSON(osm_data, { onEachFeature: onEachFeature });
      }

      LAYERS.push(l);
      map.fitBounds(l.getBounds());

      console.log("Features found:" + osm_data.features.length);
      reply(
        "Found " + osm_data.features.length + " " + data.context.element + " in " + data.context.area +
        " with a total area of " + turf.area(osm_data).toFixed(2) + "sqm"
      );
      return;
		})
    .fail(function() {
      spinner.stop();
      reply("I didn't understand the question :\\ Can you try rephrasing?");
    });
}
