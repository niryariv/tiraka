import subprocess as sp
import overpy
import geocoder
from textblob.inflect import singularize
import json

from flask import Flask, make_response, jsonify, send_file

from osm_tags import OSM_TAGS
from luis import luis_query
from local_settings import *

api = overpy.Overpass()
app = Flask(__name__)


def _get_bbox(location):
    print ("geocoding %s" % location)
    g = geocoder.google(location, key=GOOGLE_KEY)
    print(g.json)

    if g.json['status'] != "OK":
        return false

    sw, ne = g.bbox['southwest'], g.bbox['northeast']
    return (sw[0], sw[1], ne[0], ne[1])


def _get_element(search_string):
    print("_get_element(%s)" % search_string)

    # try for keys first
    search_string = search_string.replace(' ','_')
    for t in OSM_TAGS['tags']:
        if 'value' not in t and t['key'] == search_string:
            print (t)
            return(t)

        if 'value' in t and t['value'] == search_string:
            print (t)
            print("KEY: %s VALUE %s" % (t['key'], t['value']))
            return (t)


def _luis_process_query(query_text):
    d = luis_query(query_text)
    print("LUIS: ", d)
    # ignore intent for now
    e = d['entities']

    if 'location' not in e:
        e['location'] = False

    if 'object_type' not in e:
        e['object_type'] = False

    if 'render_style' not in e:
        e['render_style'] = False

    return(e['object_type'], e['location'], e['render_style'])


def _json_response(data):
    if type(data) is dict:
        data = json.dumps(data, ensure_ascii=False)

    r = make_response(data)
    r.headers['Access-Control-Allow-Origin'] = "*"
    r.headers['Content-Type'] = "application/json; charset=utf-8"
    return r

################################################################################
@app.route("/parse_query/<text>")
def parse_query(text):
    # return intent + entities
    l = luis_query(str(text))
    print(l)
    return(_json_response(l))


@app.route("/get_location/<location>")
def get_location(location):
    # return location name, center point, boundaries
    print ("geocoding %s" % location)
    g = geocoder.google(location, key=GOOGLE_KEY)
    print(g.json)

    if g.json['status'] != "OK":
        return (_json_response({'message':'Could not geocode %s' % location}))

    resp = {
        'name': g.json['address'],
        'n' : g.json['bbox']['northeast'][0],
        'e' : g.json['bbox']['northeast'][1],
        's' : g.json['bbox']['southwest'][0],
        'w' : g.json['bbox']['southwest'][1],
        'lat': g.json['lat'],
        'lng': g.json['lng']
    }

    return (_json_response(resp))

@app.route("/intent_show_objects/<n>,<e>,<s>,<w>,<object_type>")
def intent_show_objects(n,e,s,w, object_type):
    # return geoJSON of object in bbox
    coords = "%s,%s,%s,%s" % (s,w,n,e)

    t = _get_element(singularize(object_type))
    if len(t) == 2:
        tag = '"%s"="%s"' % (t['key'], t['value'])
    else:
        tag = '"%s"' %t['key']

    query = '''
        [out:json];
        (
            node[{1}]({0});
            way[{1}]({0});
            relation[{1}]({0});
        );
        out body;>;
        out skel qt;
    '''.format(coords, tag)

    query = query.replace("\n", "").strip()
    out = "echo \"%s\" | query-overpass" % query
    print(out)

    osm_data = sp.check_output(out, shell=True).decode("utf-8")

    resp = { 'osm_data': osm_data }
    print (resp)

    return(_json_response(resp))


# ################################################################################
# @app.route("/query/<query_text>")
# def query(query_text):
#     print (query_text)
#
#     element, area, render_style = _luis_process_query(str(query_text))
#
#     if not element:
#         return(_json_response({"message": "Couldn't find a feature type in your text"}))
#     if not area:
#         return(_json_response({"message": "Couldn't find a location in your text"}))
#
#     if not render_style:
#         render_style = ''
#
#     bbox = _get_bbox(area)
#
#     if not bbox:
#         return(_json_response({"message": "Couldn't find coordinates for %s" % area}))
#
#     coords = "%s,%s,%s,%s" % bbox
#
#     t = _get_element(singularize(element))
#     if len(t) == 2:
#         tag = '"%s"="%s"' % (t['key'], t['value'])
#     else:
#         tag = '"%s"' %t['key']
#
#     query = '''
#     [out:json];
#     (
#     node[{1}]({0});
#     way[{1}]({0});
#     relation[{1}]({0});
#     );
#     out body;>;
#     out skel qt;
#     '''.format(coords, tag)
#
#     query = query.replace("\n", "").strip()
#     out = "echo \"%s\" | query-overpass" % query
#     print(out)
#
#     osm_data = sp.check_output(out, shell=True).decode("utf-8")
#
#     resp = {
#         'osm_data': osm_data,
#         'context': {
#             'area': area,
#             'element': element,
#             'render_style': render_style
#         }
#     }
#
#     print (resp)
#
#     return(_json_response(resp))
#

@app.route("/")
def hello():
    return "Hello World!"


if __name__ == "__main__":
    app.run()
