from local_settings import *
import requests


LUIS_URL = 'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/96ea4855-f0d4-4ead-bfff-6ba867085a6d?subscription-key=%s&verbose=true&q=' % LUIS_SUBSCRIPTION_KEY

def _send_query(query_text):
    url = LUIS_URL + query_text.lower().replace('+',' ')
    r = requests.get(url)
    return(r.json())

def _process_data(resp_json):
    intent = resp_json['topScoringIntent']['intent']
    entities = {}
    for e in resp_json['entities']:
        entities[e['type']] = e['entity']
    return(intent, entities)

def luis_query(query):
    intent, entities = _process_data(_send_query(query))
    return({'intent': intent, 'entities': entities})

if __name__ == '__main__':
    query = "Show me a map of playgrounds in Jerusalem"
    print(luis_query(query))
