import os
import tiraka
import unittest
import tempfile
import json

class FlaskrTestCase(unittest.TestCase):

    def setUp(self):
        tiraka.app.config['TESTING'] = True
        self.app = tiraka.app.test_client()
    #     # with flaskr.app.app_context():
    #     #     flaskr.init_db()
    #
    # def tearDown(self):
    #     # os.close(self.db_fd)
    #     # os.unlink(flaskr.app.config['DATABASE'])

    # def test_hello(self):
    #     rv = self.app.get('/')
    #     assert b'Hello World' in rv.data

    def test_parse_query(self):
        rv = self.app.get('/parse_query/bars+in+harlem')
        # {'entities': {'location': 'harlem', 'object_type': 'bars'}, 'intent': 'Show Objects'}
        j = json.loads(rv.data.decode('utf-8'))
        assert (j['intent'] == 'Show Objects'           and
                j['entities']['location'] == 'harlem'   and
                j['entities']['object_type'] == 'bars'
        )

    def test_get_location(self):
        rv = self.app.get('/get_location/harlem')
        j = json.loads(rv.data.decode('utf-8'))
        #{'state': 'NY', 'status_code': 200, 'confidence': 7, 'encoding': 'utf-8', 'accuracy': 'APPROXIMATE',
        #'ok': True, 'county': 'New York County', 'country': 'US', 'status': 'OK', 'quality': 'neighborhood',
        #'bbox': {'northeast': [40.834443, -73.9338182], 'southwest': [40.7968871, -73.9623589]},
        #'address': 'Harlem, New York, NY, USA', 'lat': 40.8115504, 'place': 'ChIJn6KIIW72wokRLnDiCe-vCLQ',
        #'location': 'harlem', 'provider': 'google', 'neighborhood': 'Harlem', 'sublocality': 'Manhattan',
        #'lng': -73.9464769, 'city': 'New York'}
        assert (j['name'] == 'Harlem, New York, NY, USA' and
                j['n'] == 40.834443     and
                j['e'] == -73.9338182   and
                j['s'] == 40.7968871    and
                j['w'] == -73.9623589   and
                j['lat'] == 40.8115504  and
                j['lng'] == -73.9464769
        )

    def test_intent_show_objects(self):
        rv = self.app.get("/intent_show_objects/40.834443,-73.9338182,40.7968871,-73.9623589,bars")
        j = json.loads(rv.data.decode('utf-8'))
        d = json.loads(j['osm_data'])
        print(len(d['features']))
        assert (d['type'] == 'FeatureCollection' and
                len(d['features']) >= 2
        )


if __name__ == '__main__':
    unittest.main()
