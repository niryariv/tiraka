# -*- coding: utf-8 -*-

import overpy
import subprocess as sp

api = overpy.Overpass()

query = '''
[out:json];
node(31.7,35,31.8,35.3)[amenity=bar];
out;
'''

# result = api.query(query)
# print(result)
query = query.replace("\n", "").strip()

out = "echo \"%s\" | query-overpass" % query
print(out)
json = sp.check_output(out, shell=True)
print(json.decode("utf-8", "strict"))
